/***  imports ****/
var http = require('http');
var express = require('express')
var mongo = require('mongodb'); 
var mongoose = require('mongoose');
var dateFormat = require('dateformat');

/*** Params ****/
const hostname = '127.0.0.1';
const port = 3000;

/**** Inits ***/
var app = express();
app.set("view engine","vash")
/* body parser */
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
var mongo_client = require('mongodb').MongoClient;
var mongo_url = 'mongodb://127.0.0.1/';
var mongo_db = 'taskmanager';
var mongo_task_collection = "tasks";
var mongo_event_collection = "events";

/* Create db models */
var taskModel;
var taskSchema = new mongoose.Schema({
    taskname: String,
    task_desc: String,
    is_completed: Boolean
});
var eventModel;
var eventSchema = new mongoose.Schema({
    event_datetime: String,
    event_desc: String,
});
/* Open db */
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
mongoose.connect(mongo_url + mongo_db, {useNewUrlParser: true});
db.once('open', function() {
    taskModel = mongoose.model(mongo_task_collection, taskSchema);
    eventModel = mongoose.model(mongo_event_collection, eventSchema);
});    

/*** Routes ***/
app.get('/', function(req, res) {
    taskModel.find(function (err, tasks) {
        if (err) return console.error(err);
        eventModel.find({}).sort({event_datetime: 'desc'}).exec(function (err, events) {
            if (err) {
                res.render('index', { 
                    tasks: tasks,
                    events: null
                });
            } else {
                res.render('index', { 
                    tasks: tasks,
                    events: events
                });
            }
        });
    });
})
app.post('/api/upsert', function(req, res) {
    var task_name = req.body.taskname;
    var task_desc = req.body.taskdesc;
    var is_completed_str = req.body.is_completed;
    var is_completed = (is_completed_str !== null && is_completed_str == "on");
    try {
        mongo_client.connect(mongo_url, function(err, db) {
            if (err) throw err;
            var dbo = db.db(mongo_db);
            var result = dbo.collection(mongo_task_collection).updateOne(
                { "taskname" : task_name },
                { $set: { 
                    "task_desc" : task_desc, 
                    "is_completed" : is_completed 
                } },
                { upsert: true } // if not exists, just add it instead. 
            );
            if(result === null || result.modifiedCount == 0) {
                console.log("Could not save task, nothing created");
            } else {
                // Add event
                dbo.collection(mongo_event_collection).insertOne(
                { 
                    "event_datetime" : getDateTime(),
                    "event_desc" : "Inserted or updated task -> " + task_name + ", value:" + task_desc + ", is_completed:" + is_completed
                });
                db.close();
                res.redirect('/');
            }
        });
    } catch (e) {
        console.log(e);
    }
})
app.post('/api/delete', function(req, res) {
    var task_name = req.body.taskname;
    try {
        mongo_client.connect(mongo_url, function(err, db) {
            if (err) throw err;
            var dbo = db.db(mongo_db);
            var result = dbo.collection(mongo_task_collection).deleteOne(
                { "taskname" : task_name }
            );
            if(result === null || result.modifiedCount == 0) {
                console.log("Could not delete task, nothing to delete.");
            } else {
                // Add event
                dbo.collection(mongo_event_collection).insertOne(
                { 
                    "event_datetime" : getDateTime(),
                    "event_desc" : "Removed task " + task_name
                });
                db.close();
                res.redirect('/');
            }
        });
    } catch (e) {
        console.log(e);
    }
})
/* End routes */

function getDateTime() {
    return dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
}

app.listen(port, hostname,  () => 
    console.log(`Server running at http://${hostname}:${port}/`
));